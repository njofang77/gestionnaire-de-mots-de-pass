# Gestionnaire de mots de passe

Ce programme simple vous permet de gérer vos mots de passe en toute sécurité. Vous pouvez stocker des mots de passe, générer de nouveaux mots de passe et afficher votre liste de mots de passe enregistrés.

## Fonctionnalités

- **Générer un mot de passe maître :** Vous pouvez générer un mot de passe maître qui sera utilisé pour chiffrer vos mots de passe stockés.
- **Sauvegarder un nouveau mot de passe :** Vous pouvez enregistrer un nouveau nom d'utilisateur, un mot de passe et une adresse web dans le gestionnaire.
- **Générer un nouveau mot de passe :** Vous pouvez générer un nouveau mot de passe aléatoire de la longueur souhaitée.
- **Afficher la liste des mots de passe enregistrés :** Vous pouvez afficher la liste des mots de passe stockés après les avoir déchiffrés.

## Comment utiliser

1. Exécutez le programme en utilisant Python.

   ```
   python votre_script.py
Si le fichier vault.txt et master.key n'existent pas, le programme vous guidera pour générer un mot de passe maître et un fichier de coffre.

Une fois que vous avez un mot de passe maître et un fichier de coffre, vous pouvez choisir parmi les options suivantes :

1 : Sauvegarder un nouveau mot de passe
2 : Générer un nouveau mot de passe
3 : Afficher la liste des mots de passe enregistrés
Exigences
Ce programme nécessite l'installation de la bibliothèque cryptography, que vous pouvez installer en utilisant pip :


pip install cryptography
Avertissement
Assurez-vous de garder votre mot de passe maître en sécurité, car il est utilisé pour chiffrer et déchiffrer vos mots de passe stockés. Ne le perdez pas, car il ne peut pas être récupéré
