import os.path
import random
import sys
import subprocess
import string
import cryptography.fernet

def generateMasterPassword():
    key = cryptography.fernet.Fernet.generate_key()
    with open("./master.key", "wb") as masterPasswordWriter:
        masterPasswordWriter.write(key)
    
def loadMasterPassword():
    return open("./master.key", "rb").read()

def createVault():
    vault = open("./vault.txt"," wb")
    vault.close()

def encryptData(data):
    f = cryptography.fernet.Fernet(loadMasterPassword)
    with open("./vault.txt","rb") as vaultReader:
        encryptedData = vaultReader.read()
    if encryptedData.decode() =='':
        return f.encrypt(data.encode())
    else:
        decryptedData = f.decrypt(encryptedData)#
        newData = decryptedData.decode() + data
        return f.encrypt(newData.encode())
    
def decryptedData(encryptedData):
    f = cryptography.fernet.Fernet(loadMasterPassword)
    return f.decrypt(encryptData)

def appendNewPassword():
    print()
    userName = input("enter user name : ")
    password = input("enter your password : ")
    website = input("enter your web address : ")
    print()

    userNameLine ="user name : " + userName +"\n"
    passwordLine ="password : " + password +"\n"
    websiteLine ="web address : " + website +"\n"

    encryptedData = encryptData(userNameLine + passwordLine + websiteLine )
    with open("./vault.txt", "rb") as vaultWriter:
        vaultWriter.write(encryptedData)

def readPassword():
    with open("./vault.txt", "rb") as passwordWriter:
        encryptedData = passwordWriter.read()
        print()
        print(decryptedData(encryptedData).decode())

def generateNewPassword(passwordLength):
    randomString = string.ascii_letters + string.digits + string.punctuation
    newPassword = ""
    for i in range(passwordLength):
        newPassword += random.choice(randomString)
        print()
        print("this is your password : "+ newPassword)


subprocess.call("clear", shell=True)

print("-" * 60)
print("welome to the password manager")
print("-" * 60)

if os.path.exists("./vault.txt") and os.path.exists("./master.key"):
    print("you selected one next option : \n")
    print("1 - save password\n")
    print("2 - generate a new password : \n")
    print("3 - get the list of passwords. : \n")

    userChoice = input("que souhaitez vous faire? 1/2/3")

    if userChoice == "1":
        appendNewPassword()
    elif userChoice == "2":
        passwordLength = print("give the lenght")
        if not (string.ascii_letters in passwordLength):
            generateNewPassword(int(passwordLength))
        else:
            print("enter a number next time...")
            sys.exit()
    elif userChoice == "3":
        readPassword()
    else:
        print("the option does not exist")
        sys.exit()
else:
    print("generate a master password and coffee of password...")
    generateMasterPassword()
    createVault()
    print("generation end, please restart program")

    


    
    





